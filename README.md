# What is RMMTOS?

RMMTOS is a hobby operating system that does multitasking in real mode. It is aimed at Intel 8088 and x86 processors. It currently has no protection features as they are not supported in real mode.

# Planned features

-   Multitasking
	-   64 KB per program
	-   a managed language for safeish programs without CPU protection features?
-   Filesystem support
    -   FAT
    -   ISO9660 (?)
-   Drivers
    -   Keyboard
    -   Text mode display
    -   Floppy drives
    -   Hard disks
-   Drivers under consideration
    -   CD-drives
    -   Serial port
    -   Paraller port
    -   Mouses
    -   Some graphics modes
    -   USB media (memory, keyboards, mouses)
    -   Networking

# About done

-   keyboard driver
-   VFS
-   devfs driver
-   driver interface
-   IRQ and exception handling

# Work in progress

-   console driver
-   scheduler
-   floppy driver
-   FAT driver
-   Command prompt
-   Some programs (cat, rm, cp, etc.)

