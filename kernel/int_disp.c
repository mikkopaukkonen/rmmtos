#include "int.h"
#include "panic.h"
#include "task.h"

void intr_quick();

void stub_exception_0();
void stub_exception_1();
void stub_exception_2();
void stub_exception_3();
void stub_exception_4();
void stub_exception_5();
void stub_exception_6();
void stub_exception_7();

void stub_irq_0();
void stub_irq_1();
void stub_irq_2();
void stub_irq_3();
void stub_irq_4();
void stub_irq_5();
void stub_irq_6();
void stub_irq_7();
void stub_irq_8();
void stub_irq_9();
void stub_irq_10();
void stub_irq_11();
void stub_irq_12();
void stub_irq_13();
void stub_irq_14();
void stub_irq_15();

void stub_syscall();

struct IrqWait
{
	/* either set -> not fired, 0 -> fired */
	struct Task* task;
	struct IrqWait* next;
};

struct IrqWait* discarded_waits = 0;
struct IrqWait** irq_events[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

intr_func_t* exception_table[8] = {0,0,0,0,0,0,0,0};
intr_func_t* irq_table[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
intr_func_t* syscall_func = 0;

void irq_set_wakeup(struct Task* task, int irq)
{
	struct IrqWait* wait = malloc(sizeof(*wait));
	struct IrqWait** wait_list = &irq_events[irq];

	if(irq >= 16)
		panic("irq index out of range");

	if(!wait)
		panic("irq wait alloc failed");

	/*printf("task %x waiting for irq %d\n", task, irq);
		*/
	wait->task = task;

	if(!(*wait_list))
		wait->next = 0;
	else
		wait->next = (*wait_list);

	(*wait_list) = wait;

	while(discarded_waits) {
		wait = discarded_waits;

		free(wait);

		discarded_waits = discarded_waits->next;
	}
}

void irq_resume_waiting_tasks(int irq)
{
	struct IrqWait* item = irq_events[irq];
	struct IrqWait* last = 0;

	while(item)
	{
		task_wakeup(item->task);

		if(!item->next)
			last = item;

		item = item->next;
	}

	if(last) {
		last->next = discarded_waits;
		discarded_waits = irq_events[irq];
	}

	irq_events[irq] = 0;
}

void intr_set_handler(unsigned int index,unsigned int cs,unsigned int ip)
{
	if(index > 0xff)
		panic("index out of range");

	pokew(0, index * 4, ip);
	pokew(0, index * 4 + 2, cs);
}

void exception_handler(unsigned int num)
{
	intr_func_t* func = exception_table[num];

	if(func)
	{
		func(num);
	}
	else
	{
		printf("unhandled exception %x\n", num);
		panic("");
	}
}

#define PIC_EOI 0x20
#define PIC1_CMD 0x20
#define PIC2_CMD 0xA0
#define PIC_READ_ISR 0x0B

void irq_primary(unsigned int num) {
	task_check_kernel_stack();

	if(irq_table[num]) {
		irq_table[num](num);
	}

	irq_resume_waiting_tasks(num);

	outb(PIC1_CMD, PIC_EOI);

	task_check_kernel_stack();
}

void irq_primary_spurious(unsigned int num) {
	unsigned char isr;

	task_check_kernel_stack();

	if(irq_table[num]) {
		irq_table[num](num);
	}

	outb(PIC1_CMD, PIC_READ_ISR);

	isr = inb(PIC1_CMD);

	if(isr & 0x8) {
		irq_resume_waiting_tasks(num);

		outb(PIC1_CMD, PIC_EOI);
	}

	task_check_kernel_stack();
}

void irq_secondary(unsigned int num) {
	task_check_kernel_stack();

	if(irq_table[num]) {
		irq_table[num](num);
	}

	irq_resume_waiting_tasks(num);

	outb(0xA0, 0x20);
	outb(0x20, 0x20);

	task_check_kernel_stack();
}

void irq_secondary_spurious(unsigned int num) {
	unsigned char isr;

	task_check_kernel_stack();

	if(irq_table[num]) {
		irq_table[num](num);
	}

	outb(PIC2_CMD, PIC_READ_ISR);

	isr = inb(PIC2_CMD);

	if(isr & 0x8) {
		irq_resume_waiting_tasks(num);

		outb(0xA0, 0x20);
	}

	outb(0x20, 0x20);

	task_check_kernel_stack();
}

void intr_init()
{
	int i;

	for(i = 0; i < 256; i++)
		intr_set_handler(i, 0x0050, intr_quick);

	intr_set_handler(0x00, 0x0050, stub_exception_0);
	intr_set_handler(0x01, 0x0050, stub_exception_1);
	intr_set_handler(0x02, 0x0050, stub_exception_2);
	intr_set_handler(0x03, 0x0050, stub_exception_3);
	intr_set_handler(0x04, 0x0050, stub_exception_4);
	intr_set_handler(0x05, 0x0050, stub_exception_5);
	intr_set_handler(0x06, 0x0050, stub_exception_6);
	intr_set_handler(0x07, 0x0050, stub_exception_7);

	intr_set_handler(0x08, 0x0050, stub_irq_0);
	intr_set_handler(0x09, 0x0050, stub_irq_1);
	intr_set_handler(0x0a, 0x0050, stub_irq_2);
	intr_set_handler(0x0b, 0x0050, stub_irq_3);
	intr_set_handler(0x0c, 0x0050, stub_irq_4);
	intr_set_handler(0x0d, 0x0050, stub_irq_5);
	intr_set_handler(0x0e, 0x0050, stub_irq_6);
	intr_set_handler(0x0f, 0x0050, stub_irq_7);

	intr_set_handler(0x70, 0x0050, stub_irq_8);
	intr_set_handler(0x71, 0x0050, stub_irq_9);
	intr_set_handler(0x72, 0x0050, stub_irq_10);
	intr_set_handler(0x73, 0x0050, stub_irq_11);
	intr_set_handler(0x74, 0x0050, stub_irq_12);
	intr_set_handler(0x75, 0x0050, stub_irq_13);
	intr_set_handler(0x76, 0x0050, stub_irq_14);
	intr_set_handler(0x77, 0x0050, stub_irq_15);

	intr_set_handler(0x80, 0x0050, stub_syscall);
}

int irq_capture(unsigned int irq, intr_func_t* func)
{
	if(irq >= 16)
		panic("irq out of range");

	if(irq_table[irq])
		return 0;

	irq_table[irq] = func;

	return 1;
}

int exception_capture(unsigned int exc, intr_func_t* func)
{
	if(exc >= 8)
		panic("exception out of range");

	if(exception_table[exc])
		return 0;

	exception_table[exc] = func;

	return 1;
}

void sys_capture(intr_func_t* func)
{
	syscall_func = func;
}

void syscall_dispatch() {

	asm("sti");

	task_check_kernel_stack();

	if(syscall_func)
		syscall_func(0);

	task_check_kernel_stack();
}

void exception_dispatch(unsigned int num)
{
	task_check_kernel_stack();

	exception_handler(num);

	task_check_kernel_stack();
}
