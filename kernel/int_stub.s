import kernel_enter
import _kernel_exit

import _exception_dispatch

MACRO EXCEPTION
export _stub_exception_?1
_stub_exception_?1:
	call kernel_enter

	mov ax, #?1
	push ax
	call _exception_dispatch
	pop ax
	jmp word _kernel_exit
MEND

MACRO IRQ
export _stub_irq_?1
_stub_irq_?1:
	call kernel_enter

	mov ax, #?1
	push ax
	call ?2
	pop ax
	jmp word _kernel_exit
MEND

import _syscall_dispatch

MACRO SYS
export _stub_syscall
_stub_syscall:
	call kernel_enter

	call _syscall_dispatch
	jmp word _kernel_exit
MEND

; for exceptions
EXCEPTION 0
EXCEPTION 1
EXCEPTION 2
EXCEPTION 3
EXCEPTION 4
EXCEPTION 5
EXCEPTION 6
EXCEPTION 7

import _irq_primary
import _irq_primary_spurious
import _irq_secondary
import _irq_secondary_spurious

;for irqs 0-15
IRQ(0, _irq_primary)
IRQ(1, _irq_primary)
IRQ(2, _irq_primary)
IRQ(3, _irq_primary)
IRQ(4, _irq_primary)
IRQ(5, _irq_primary)
IRQ(6, _irq_primary)
IRQ(7, _irq_primary_spurious)
IRQ(8, _irq_secondary)
IRQ(9, _irq_secondary)
IRQ(10, _irq_secondary)
IRQ(11, _irq_secondary)
IRQ(12, _irq_secondary)
IRQ(13, _irq_secondary)
IRQ(14, _irq_secondary)
IRQ(15, _irq_secondary_spurious)

;for syscall
SYS

export _intr_quick
_intr_quick:
	iret
