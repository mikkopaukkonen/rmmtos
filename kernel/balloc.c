#include "balloc.h"
#include "panic.h"

// Block size 512 bytes
#define BLOCK_SIZE_POT 9
#define SEG_SIZE_POT 4
#define BLOCK_SIZE_MASK ~(-1 << 9)
#define BITMAP_SIZE 160
#define total_blocks 1280

//TODO: check if really zero allocated
unsigned char freemap[BITMAP_SIZE] = {0};
unsigned char lastmap[BITMAP_SIZE] = {0};

#define NOTFOUND -1

unsigned int balloc(unsigned long size)
{
	unsigned int alloc_block = NOTFOUND;

    if(size & BLOCK_SIZE_MASK) {
        panic ("NIMPL");
    }

    size >>= BLOCK_SIZE_POT;

    // Find `size` consecutive free blocks
    {
        int free_found = 0;
        int current_block = 0;

        while(freemap[current_block / 8] == 0xff && current_block < total_blocks) {
            // move to first block of next byte
            current_block = (current_block + 8) & (~0x7);
        }

        while (free_found < size && current_block < total_blocks) {
            if((freemap[current_block / 8] & (0x80 >> (current_block % 8))) == 0) {
                printf("bf%x ", current_block);

                // free
                free_found++;
                
                if(alloc_block == NOTFOUND) {
                    alloc_block = current_block;
                }

            } else {
                printf("bu%x ", current_block);
                // not free
                free_found = 0;
                alloc_block = NOTFOUND;
            }
		    
            current_block++;
        }
    }

    // Mark the block as used
    if (alloc_block != NOTFOUND)
    {
        int i;
        int current_block = alloc_block;

        for (i = 0; i < size; ++i) {
            freemap[current_block / 8] |= (0x80 >> (current_block % 8));
        }

        // mark the last block as being the last block of the allocation
        current_block--;
        lastmap[current_block / 8] |= (0x80 >> (current_block % 8));

        printf("balloc: %d -> %x\n", (unsigned int)size, alloc_block);
        printf("balloc: %d -> %x\n", (unsigned int)size, alloc_block << (BLOCK_SIZE_POT - SEG_SIZE_POT));

        return alloc_block << (BLOCK_SIZE_POT - SEG_SIZE_POT);
    }

	printf("balloc: %d\n", size);
	panic("out of mem");

	return 0;
}

void bfree(unsigned int block)
{
	unsigned int current_block = block >> (BLOCK_SIZE_POT - SEG_SIZE_POT);
	int last = 0;

    do {
        if (lastmap[current_block / 8] & (0x80 >> (current_block % 8))) {
            last = 1;
        }

        freemap[current_block / 8] ^= (0x80 >> (current_block % 8));
        lastmap[current_block / 8] ^= (0x80 >> (current_block % 8));

        current_block++;
    } while(!last);

	debug_printf("bfree %x\n", block, current_block);
}

void balloc_reserve(unsigned long start, unsigned long end) 
{
    debug_printf("bres %x%x %x%x --- ", (unsigned int)(start>>16), (unsigned int)start, (unsigned int)(end>>16), (unsigned int)end);
    
    start = start >> BLOCK_SIZE_POT;
    end = end >> BLOCK_SIZE_POT;

    debug_printf("bres %x%x %x%x --- ", (unsigned int)(start>>16), (unsigned int)start, (unsigned int)(end>>16), (unsigned int)end);
    
    while (start <= end) {
        debug_printf("%x ", (unsigned int)start);
        freemap[start / 8] |= (0x80 >> (start % 8));
        start++;
        // Do not bother setting lastmap, as reserved blocks should not be free'd anyway.        
    }

    debug_printf("\n");
}

void balloc_init()
{
	memset(freemap, 0, BITMAP_SIZE);
    memset(lastmap, 0, BITMAP_SIZE);
}

