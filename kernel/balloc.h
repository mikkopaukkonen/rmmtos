#ifndef __BALLOC_H
#define __BALLOC_H

#define HZ 20
#define TICK (1000 / HZ)

unsigned int balloc(unsigned long size);
void bfree(unsigned int block);
void balloc_init();
void balloc_reserve(unsigned long start, unsigned long end);

#endif

